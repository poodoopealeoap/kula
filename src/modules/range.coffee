Kula.prototype.range = (from, to, resolution = 1, resolutionType = "days") ->
	type = from.constructor.name.toLowerCase()
	values = []
	if to is undefined or to is null
		to = from
	if to.constructor.name.toLowerCase() isnt type
		throw "kula.range() error: 'from' and 'to' types do not match."
		return
	switch type
		when "number"
			if from <= to
				while from <= to
					values.push from
					from += resolution
			else
				while from >= to
					values.push from
					from -= resolution
		when "string"
			from = alphabet.indexOf(from[0])
			to = alphabet.indexOf(to[0])
			if from is -1
				from = 0
			if to is -1
				to = 0
			if from <= to
				while from <= to
					values.push alphabet[from]
					from += Math.round(resolution)
			else
				while from >= to
					values.push alphabet[from]
					from -= Math.round(resolution)
		when "date"
			resolution = Math.round resolution
			if from <= to
				while from <= to
					values.push new Date(from.toString())
					switch resolutionType.toLowerCase()
						when "hours" then from.setHours(from.getHours() + resolution)
						when "days" then from.setDate(from.getDate() + resolution)
						when "months" then from.setMonth(from.getMonth() + resolution)
						when "years" then from.setFullYear(from.getFullYear() + resolution)
			else
				while from >= to
					values.push new Date(from.toString())
					switch resolutionType.toLowerCase()
						when "hours" then from.setHours(from.getHours() - resolution)
						when "days" then from.setDate(from.getDate() - resolution)
						when "months" then from.setMonth(from.getMonth() - resolution)
						when "years" then from.setFullYear(from.getFullYear() - resolution)
			values.toDateStrings = (pattern) ->
 			i = 0
 			l = this.length
 			convertedVals = []
 			while i < l
 				convertedVals.push kula.date(pattern, this[i])
 				i++
 			convertedVals
	values