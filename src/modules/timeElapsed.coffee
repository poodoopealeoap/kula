Kula.prototype.timeElapsed = (options = {from: new Date(), to: new Date(), in: "ms"}) ->
	if options.from is undefined or options.from is null then options.from = new Date()
	if typeof options.from is "string" or typeof options.from is "number" then options.from = new Date(options.from) else if typeof options.from is "object" and options.from.constructor.name isnt "Date" then options.from = new Date()
	if options.to is undefined or options.to is null then options.to = new Date()
	if typeof options.to is "string" or typeof options.to is "number" then options.to = new Date(options.to) else if typeof options.to is "object" and options.to.constructor.name isnt "Date" then options.to = new Date()
	if options.in is undefined or options.in is null then options.to is "ms"
	if typeof options.in isnt "string" then options.to is "ms"
	diff = Math.abs Date.parse(options.from) - Date.parse(options.to)
	switch options.in.toLowerCase()
		when "ms", "milli", "millisecond", "milliseconds" then diff
		when "s", "sec", "second", "seconds" then Math.floor(diff / 1000)
		when "i", "min", "minute", "minutes" then Math.floor((diff / 1000) / 60)
		when "h", "hr", "hrs", "hour", "hours" then Math.floor(((diff / 1000) / 60) / 60)
		when "d", "day", "days" then Math.floor((((diff / 1000) / 60) / 60) / 24)
		when "w", "wk", "wks", "week", "weeks" then Math.floor(((((diff / 1000) / 60) / 60) / 24) / 7)
		when "m", "mo", "mos", "month", "months" then Math.floor((((((diff / 1000) / 60) / 60) / 24) / 365) * 12)
		when "y", "yr", "yrs", "year", "years" then Math.floor(((((diff / 1000) / 60) / 60) / 24) / 365)