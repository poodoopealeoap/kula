Kula.prototype.lightLevel = (color) ->
	max = 255 + 255 + 255
	format = getColorFormat color
	r = 0
	g = 0
	b = 0
	if format is "rgb" or format is "rgba"
		color = if color.match kula.regex.rgb then color.replace("rgb(","") else color.replace("rgba(","")
		color = color.replace(")","").split(",")
		r = parseInt color[0]
		g = parseInt color[1]
		b = parseInt color[2]
	else if format is "hex"
		r = hex.indexOf color.substring(1,3).toLowerCase()
		g = hex.indexOf color.substring(3,5).toLowerCase()
		b = hex.indexOf color.substring(5,7).toLowerCase()
	else
		return console.error "Invalid color format"
	((r + g + b) / max) * 100