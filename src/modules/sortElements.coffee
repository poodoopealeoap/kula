Kula.prototype.sortElements = (elements, sortBy = "text", direction = "asc") ->
	if typeof elements is "string" then elements = $(elements)
	sortBy = sortBy.toLowerCase()
	direction = direction.toLowerCase()
	sortArray = []

	elements.sort (a,b) ->
		valA = if sortBy is "text" then $(a).text() else $(a).attr(sortBy)
		valB = if sortBy is "text" then $(b).text() else $(b).attr(sortBy)
		if valA < valB
			return if direction is "desc" then 1 else -1
		if valA > valB
			return if direction is "desc" then -1 else 1
		return 0
	.each ->
		$(@).parent().append $(@)
			
	return