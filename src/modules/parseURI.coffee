Kula.prototype.parseURI = (format, uri = window.location.pathname.substr 1) ->
	if format
		format = if format[0] is "/" then format.substr 1 else format
		format = format.split("{").join("").split("}").join("")
	return kula.parseDelimitedString uri, "/", format