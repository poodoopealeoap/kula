Kula.prototype.parseDelimitedString = (delimitedString, delimiter, format) ->
    if not delimitedString
        return null
    if not delimiter
        return delimitedString
    keys = if format then format.split delimiter else format
    values = delimitedString.split delimiter
    result = if keys then {} else []
    if keys
        keys.forEach (key, i) ->
            result[key] = values[i];
        return result
    values