Kula.prototype.parseQS = (url = window.location.href) ->
	values = {}
	if url.indexOf("?") isnt -1
		qs = url.split("?")[1].split("&")
		i = 0
		while i < qs.length
			values[qs[i].split("=")[0]] = qs[i].split("=")[1]
			i++
	values