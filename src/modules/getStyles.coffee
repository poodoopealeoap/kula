Kula.prototype.getStyles = (selector) ->
	element = buildElement selector
	$("body").append "<div id=\"RipStyle-wrapper\"> style=\"position:absolute;top:-999999px;left:-999999px;\">#{element}</div>"
	styles = {}
	computedStyles = getComputedStyle $("#RipStyle-wrapper #{selector}")[0]
	for prop, val of computedStyles
		styles[prop] = val
	$("#RipStyle-wrapper").remove()
	styles