Kula.prototype.getFormVals = (formID) ->
	output = {}
	querystring = if formID is null or formID is undefined then $("form").serialize().split("&") else $("form#{formID}").serialize().split("&")
	parse = (pair) ->
		output[pair.split("=")[0]] = pair.split("=")[1]
	parse pair for pair in querystring
	output