convertColor = (color, outFormat) ->
	color = color.toLowerCase()
	if color.match kula.regex.rgb
		if outFormat is null or outFormat is undefined then outFormat = "rgb"
		switch outFormat.toLowerCase()
			when "rgb"
				color
			when "rgba"
				color.replace("rgb","rgba").replace(")",",1)")
			when "hex","hexidecimal"
				"##{hex[parseInt color.split(',')[0].split('(')[1] ]}#{hex[parseInt color.split(',')[1] ]}#{hex[parseInt color.split(',')[2].split(')')[0] ]}"
	else if color.match kula.regex.rgba
		if outFormat is null or outFormat is undefined then outFormat = "rgba"
		switch outFormat.toLowerCase()
			when "rgba"
				color
			when "rgb"
				"rgb(#{color.split(',')[0].split('(')[1]},#{color.split(',')[1]},#{color.split(',')[2]})"
			when "hex", "hexidecimal"
				"##{hex[parseInt color.split(',')[0].split('(')[1] ]}#{hex[parseInt color.split(',')[1] ]}#{hex[parseInt color.split(',')[2] ]}"
	else if color.match kula.regex.hex
		if outFormat is null or outFormat is undefined then outFormat = "hex"
		switch outFormat.toLowerCase()
			when "hex", "hexidecimal"
				color
			when "rgb"
				"rgb(#{hex.indexOf color.substring 1,3},#{hex.indexOf color.substring 3,5},#{hex.indexOf color.substring 5,7})"
			when "rgba"
				"rgba(#{hex.indexOf color.substring 1,3},#{hex.indexOf color.substring 3,5},#{hex.indexOf color.substring 5,7},1)"