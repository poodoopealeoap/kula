Kula.prototype.getLineEndings = (text) ->
    result = []
    if text.match(/\r\n/)
        result.push "\r\n"
    if text.match(/[^\r]?\n/)
        result.push "\n"
    if result.length is 1
        return result[0]
    if result.length > 1
        return result
    return null