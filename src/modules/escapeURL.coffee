Kula.prototype.escapeURL = (string) ->
	if string is undefined or string is null then return string
	chars = []
	chars.push char for char in string
	convert = (char) ->
		switch char
			when "%" then "%25"
			when " " then "%20"
			when "$" then "%24"
			when "&" then "%26"
			when "`" then "%60"
			when ":" then "%3A"
			when "<" then "%3C"
			when ">" then "%3E"
			when "[" then "%5B"
			when "]" then "%5D"
			when "{" then "%7B"
			when "}" then "%7D"
			when '"' then "%22"
			when "+" then "%2B"
			when "#" then "%23"
			when "@" then "%40"
			when "/" then "%2F"
			when ";" then "%3B"
			when "=" then "%3D"
			when "?" then "%3F"
			when "\\" then "%5C"
			when "^" then "%5E"
			when "|" then "%7C"
			when "~" then "%7E"
			when "'" then "%27"
			when "," then "%2C"
			else char
	result = []
	result.push convert char for char in chars
	result.join("")