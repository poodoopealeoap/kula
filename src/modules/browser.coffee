Kula.prototype.browser = () ->
	if navigator.userAgent.match /Trident/
		"IE"
	else if navigator.userAgent.match /Safari/
		if navigator.userAgent.match /OPR/
			"Opera"
		else if navigator.userAgent.match /Chrome/
			"Chrome"
		else
			"Safari"
	else if navigator.userAgent.match /Firefox/
		"Firefox"
	else
		"Unknown"