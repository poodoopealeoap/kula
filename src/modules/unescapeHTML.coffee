Kula.prototype.unescapeHTML = (string) ->
	if string is undefined or string is null then return string
	string.split(/&sol;/g).join("/")
		.split(/&bsol;/g).join("\\")
		.split(/&#039;/g).join("'")
		.split(/&quot;/g).join('"')
		.split(/&num;/g).join("#")
		.split(/&gt;/g).join(">")
		.split(/>>/g).join(">&gt;")
		.split(/&lt;/g).join("<")
		.split(/<</g).join("&lt;<")
		.split(/&equals;/g).join("=")
		.split(/&amp;/g).join("&")