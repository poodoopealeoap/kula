Kula.prototype.getGravatar = (options) ->
	if options.email is undefined or options.email is null then return console.error "kula.getGravatar() - No email provided"
	if options.size is undefined then options.size = 80
	if options.rating is undefined then options.rating = null
	if options.default is undefined then options.default = null
	img = new Image
	src = "http://www.gravatar.com/avatar/#{kula.MD5(options.email)}?&s=#{parseInt options.size}"
	if options.rating isnt null
		src += "&r=#{options.rating}"
	if options.default isnt null
		src += "&d=#{kula.escapeURL options.default}"
	img.src = src
	img