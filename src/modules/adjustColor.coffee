adjustColor = (method, color, percent = 20, outFormat) ->
	process = (num) ->
		switch method
			when "lighten", "dodge"
				num * (100 + percent) / 100
			when "darken", "burn"
				num * (100 - percent) / 100
			when "invert"
				if num is (255 / 2) then num else 255 - num
	confineNumber = (num) -> if num > 255 then 255 else if num < 0 then 0 else num
	switch getColorFormat color
		when "rgb", "rgba"
			color = if color.match kula.regex.rgb then color.replace "rgb(","" else color.replace "rgba(",""
			color = color.replace(")","").split(",")
			r = parseInt confineNumber process parseInt color[0]
			g = parseInt confineNumber process parseInt color[1]
			b = parseInt confineNumber process parseInt color[2]
			a = if color.length is 4 then parseFloat color[3] else null
			result = if a is null then "rgb(#{r},#{g},#{b})" else "rgba(#{r},#{g},#{b},#{a})"
			convertColor result, outFormat
		when "hex"
			r = parseInt confineNumber process hex.indexOf color.substring(1,3).toLowerCase()
			g = parseInt confineNumber process hex.indexOf color.substring(3,5).toLowerCase()
			b = parseInt confineNumber process hex.indexOf color.substring(5,7).toLowerCase()
			result = "##{hex[r]}#{hex[g]}#{hex[b]}"
			convertColor result, outFormat