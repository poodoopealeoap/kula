Kula.prototype.date = (format = "m-d-Y", date) ->
	date = new Date date
	if date.toString() is "Invalid Date" then date = new Date()
	d =
		year: parseInt date.toISOString().split("-")[0]
		month: parseInt date.toISOString().split("-")[1]
		day: parseInt date.toISOString().split("-")[2].split("T")[0]
		hour: parseInt date.getHours()
		minute: parseInt date.toISOString().split(":")[1]
		second: parseInt date.toISOString().split(":")[2].split(".")[0]
		millisecond: parseInt date.toISOString().split(".")[1].split("Z")[0]
	month =
		long: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
		short: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
	day =
		long: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
		short: ["Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat"]
	chars = []
	chars.push char for char in format
	convert = (char) ->
		switch char
			when "m"
				if d.month < 10 then return "0#{d.month}"
				String d.month
			when "n"
				String d.month
			when "d"
				if d.day < 10 then return "0#{d.day}"
				String d.day
			when "j"
				String d.day
			when "Y"
				String d.year
			when "y"
				String(d.year).slice(-2)
			when "H"
				if d.hour < 10 then return "0#{d.hour}"
				String d.hour
			when "G"
				String d.hour
			when "i"
				if d.minute < 10 then return "0#{d.minute}"
				String d.minute
			when "s"
				if d.second < 10 then return "0#{d.second}"
				String d.second
			when "u"
				if d.millisecond < 10 then return "00#{d.millisecond}" else if d.millisecond < 100 then return "0#{d.millisecond}"
				d.millisecond
			when "M"
				month.short[d.month - 1]
			when "F"
				month.long[d.month - 1]
			when "a"
				if d.hour < 12 then return "am"
				"pm"
			when "A"
				if d.hour < 12 then return "AM"
				"AM"
			when "g"
				char = if d.hour < 13 then String d.hour else String (d.hour - 12)
				if char.length < 2 then return "0#{char}"
				char
			when "h"
				if d.hour < 10 then return "0#{d.hour}"
				String d.hour
			when "z"
				kula.timeElapsed({ from: "01/01/#{d.year}", to: date, in: "days" }) + 1
			when "D"
				day.short[date.getDay()]
			when "l"
				day.long[date.getDay()]
			when "w"
				date.getDay()
			when "W"
				kula.timeElapsed({ from: "01/01/#{d.year}", to: date, in: "weeks" }) + 1
			else
				char
	result = []
	result.push convert char for char in chars
	result.join("")