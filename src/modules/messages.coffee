Kula.prototype.messages = (input, state, reroute) ->
	items = []
	if typeof input is "string"
		items.push input
	else if typeof input is "object"
		items = input
	state = state.toLowerCase()
	process = (item) ->
		blank = ->
		switch item
			when "*", "all"
				if state is "off"
					if window._alert is undefined or window._alert is null then window._alert = window.alert
					if window._prompt is undefined or window._prompt is null then window._prompt = window.prompt
					if window._confirm is undefined or window._confirm is null then window._confirm = window.confirm
					if reroute isnt null or reroute isnt undefined
						window.alert = reroute
						window.prompt = reroute
						window.confirm = reroute
					else
						window.alert = blank
						window.prompt = blank
						window.confirm = blank
				else if state is "on"
					window.alert = window._alert
					window.prompt = window._prompt
					window.confirm = window._confirm
			when "alert"
				if state is "off"
					if window._alert is undefined or window._alert is null then window._alert = window.alert
					window.alert = if reroute isnt null then reroute else blank
				else if state is "on"
					window.alert = window._alert
			when "prompt"
				if state is "off"
					if window._prompt is undefined or window._prompt is null then window._prompt = window.prompt
					window.prompt = if reroute isnt null then reroute else blank
				else if state is "on"
					window.prompt = window._prompt
			when "confirm"
				if state is "off"
					if window._confirm is undefined or window._confirm is null then window._confirm = window.confirm
					window.confirm = if reroute isnt null then reroute else blank
				else if state is "on"
					window.confirm = window._confirm
	process item for item in items
	return