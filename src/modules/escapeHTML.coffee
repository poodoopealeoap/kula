Kula.prototype.escapeHTML = (string) ->
	if string is undefined or string is null then return string
	chars = []
	chars.push char for char in string
	convert = (char) ->
		switch char
			when "&" then "&amp;"
			when "=" then "&equals;"
			when "<" then "&lt;"
			when ">" then "&gt;"
			when "#" then "&num;"
			when '"' then "&quot;"
			when "'" then "&#039;"
			when "\\" then "&bsol;"
			when "/" then "&sol;"
			else char
	result = []
	result.push convert char for char in chars
	result.join("")