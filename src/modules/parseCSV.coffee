Kula.prototype.parseCSV = (text, format) ->
    lineEndings = kula.getLineEndings text
    lines = []
    if lineEndings.constructor.name is "Array"
        lines = text.split(lineEndings[0]).split(lineEndings[1])
    else if lineEndings isnt null
        lines = text.split lineEndings
    if format is undefined
        format = lines.splice(0,1)[0]
    result = []
    lines.forEach (line, i) ->
        result.push kula.parseDelimitedString line, ",", format
    result