Kula.prototype.mergeObjects = (objects = [{},{}]) ->
    if objects.constructor.name isnt "Array"
        return console.error "kula.mergeObjects() - Input value must be the type of \"Array\" and must contain only valid objects."
    result = {}
    for obj in objects
        for prop of obj
            result[prop] = obj[prop]
    result