getColorFormat = (color) ->
	if color.match kula.regex.rgb
		"rgb"
	else if color.match kula.regex.rgba
		"rgba"
	else if color.match kula.regex.hex
		"hex"
	else if color.match kula.regex.hsl
		"hsl"
	else if color.match kula.regex.hsla
		"hsla"