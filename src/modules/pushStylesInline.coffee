Kula.prototype.pushStylesInline = (selector) ->
    inject = (el) ->
        styles = getComputedStyle el
        styleString = ""
        styleString += "#{style}:#{styles[style]};" for style of styles
        el.style = styleString
        return
    inject element for element in document.querySelectorAll "#{selector}, #{selector} *"
    return