# Change log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning] (http://semver.org/).

## [2.4.1] - 2017-03-16
### Changed
- kula.getLineEndings() - previous code would only return a string value of either a windows or unix line ending but has now been updated to return an array of both line ending strings if the text is found to have mixed line endings. Otherwise, it will just return a string of which type of line ending was found, or if no line endings are found, a null value is now returned.


## [2.4.0] - 2017-03-16
### Added
- Created new kula.parseDelimitedString() method which takes a given delimited string, a delimiter, and an optional key format string and either returns an object or an array of the delimited string values.
- Created new kula.getLineEndings() method which takes a given string and returns either a string value or an array of the line endings found.
- Created new kula.parseCSV() method which takes a string value of the CSV content and an optional key format string and returns either an array of objects or an array of arrays containing the values from each row.
### Changed
- kula.parseURI() - Shortened logic to utilize new kula.parseDelimitedString() method. Also, the provided format string no longer requires there to be braces "{}" around each key name, though it will still work if the braces are still provided.


## [2.3.0] - 2017-03-10
### Added
- kula.regex - Added patterns for matching phone numbers in Canada, Mexico, and US.


## [2.2.3] - 2017-03-08
### Changed
- kula.sortElements() - revised logic to make sorting more efficient.


## [2.1.2] - 2016-09-20
### Changed
- Moved docs.js and docs.css into root of the node package so they will show up when not installing all dev dependencies and building.


## [2.1.2] - 2016-09-16
### Changed
- documentation - fixed documentation to represent the correct parameter names for kula.range().
- kula.date() - cleaned up logic for handling if an invalid date is entered as the second parameter.


## [2.1.1] - 2016-09-08
### Changed
- kula.range() - is now able to provide a range of letters and a range of dates as well as the standard range of numbers.
- kula.getKeyChar() - fixed bug where certain character keys would return undefined instead of the actual character typed.


## [2.1.0] - 2016-08-31
### Added
- kula.range() - returns an array of numbers between a minimum and a maximum number at a specified resolution.
### Changed
- Updated documentation page to include kula.range() information.


## [2.0.4] - 2016-08-24
### Changed
- kula.getKeyChar() - fixed character detection for +=_-;: characters.


## [2.0.2] - 2016-08-22
### Changed
- republished after gulp build had been run because the previous publish was missing the dist folder.


## [2.0.2] - 2016-08-22
### Changed
- updated index.html to load jQuery via cdn rather than the local dev install so it will display correctly for people who are just using the plugin and want to view the documentation.


## [2.0.1] - 2016-08-22
### Changed
- updated .npmignore file to allow the index.html documentation page to be installed with the rest of the node module.


## [2.0.0] - 2016-08-12
### Added
- index.html - Contains all documentation for the library.
- npm package now has jquery@2.2.4 included as a dependency as jQuery is a requirement for this library to run but any versions above 2.2.4 will throw errors when being used with Bootstrap 3.
- kula.regex.email - email address pattern.
### Changed
- Converted all project source files from JavaScript to CoffeeScript
- kula.currentBrowser has been changed to just kula.browser since "current" is redundant to have in the name.
- kula.escapeHTML() and kula.escapeURL() have been reworked for added reliability.
- buildStatus page has been renamed to index.html and has been completely rewritten to act as documentation for the project.
- kula.parseQS() no longer automatically unescapes values.
- kula.parseURI() now takes an optional second parameter for parsing a provided URI string rather than the default of the current page's URI.
### Removed
- buildTest.html
- tests - removed due to the majority of the methods in this library not being testable via a real javascript testing framework as well as not being able to be reliably tested in the browser since multiple methods will return different values depending on the browser.
- kula.toCamelCase() and kula.separateCamelCase() - removed due to this functionality being very basic to implement manually and these methods don't seem to be commonly needed for the majority of developers.


## [1.14.1] - 2016-07-20
### Changed
- kula.unescapeURL() - updated function to look for more than just uppercase versions of the escape characters as this was causing issues when recalling certain url strings that have been escaped by the browser itself.


## [1.14.0] - 2016-07-20
### Changed
- kula.parseQS() - updated function so that if a string is provided, it will parse that string's query string. Otherwise, no string being entered will make it work as it used to and parse the current window.location.href's query string.


## [1.13.0] - 2016-07-19
### Added
- kula.currentBrowser - a string indicating what browser the current page is loaded in. Can be any of the following: Firefox, IE, Chrome, Safari, Opera, Unknown Browser.


## [1.12.0] - 2016-07-18
### Added
- kula.lightLevel() - returns the value from 0 to 100 percent of how "light" the provided color is.


## [1.11.0] - 2016-07-18
### Added
- kula.invert() - inverts a given color and returns it.
### Removed
- cssProperties array - no longer needed with the new way that kula.getStyles() generates the object that it returns.

## [1.10.1] - 2016-07-13
### Changed
- kula.getStyles() - removed console.log() of all properties in loop which was originally in place for testing purposes.


## [1.10.0] - 2016-06-30
### Added
- kula.getStyles() - very similar to the existing getStyle() function, this new method will now return all computed styles for the element at once. This is significantly more efficient and is meant to be the replacement for getStyle() in the future. Leaving getStyle() in kula for legacy purposes at this time.


## [1.9.1] - 2016-06-30
### Changed
- kula.date() - new format characters: D = Short textual day of the week. (Sun - Sat)
									   l = Long textual day of the week. (Sunday - Saturday)
									   w = Numerical day of the week. (1 - 365)
									   W = Numerical week of the year. (1 - 52)


## [1.9.0] - 2016-06-24
### Added
- kula.separateCamelCase() - takes a string and returns a string with each 'hump' of the camel case string being separated by a given delimiter. (defaults to a space character).
- kula.toCamelCase() - takes a string and returns the string without any of the given delimiter (default is space character) and the first letter of each word other than the first in caps. (i.e. - kula.toCamelCase('this is a string') === 'thisIsAString');
### Changed
- kula.timeElapsed() - adjusted timeElapsed to allow for strings or numbers to be submitted for 'from' and 'to' in the input options rather than having to be a Date object.


## [1.8.0] - 2016-06-24
### Added
- kula.getKeyChar() - takes a keypress event and returns the actual input character. (intended to be used for keydown events so manipulations can be done prior to the character actually going into the input field on the page)
### Changed
- kula.date() - now able to take a string or a number for the date param, rather than just a Date object.


## [1.7.0] - 2016-06-19
### Changed
- kula.timeElapsed() - can now return the number of weeks.


## [1.5.3] - 2016-06-02
### Added
- kula.date() function now is able to handle returning am/pm, AM/PM, and 12 hour format hour values.
### Changed
- Reworked the entire kula.date() function so it should no longer be able to have conflicts while rendering.

## [1.5.2] - 2016-06-02
### Added
- regex to match all color types.
### Changed
- Updated various input variable names to make auto-intellisense more helpful.


## [1.5.1] - 2016-05-24
### Changed
- Reworked sortElements function to allow for sorting by text content and set this as it's default behavior.


## [1.5.0] - 2016-05-24
### Added
- sortElements function.
- sortElements test file, currently no tests implemented.


## [1.4.0] - 2016-05-23
### Added
- getFormVals function and tests.


## [1.3.0] - 2016-05-23
### Added
- getGravatar function.


## [1.2.0] - 2016-05-23
### Added
- MD5 function and tests.


## [1.1.1] - 2016-05-23
### Added
- LICENSE file.

### Changed
- Reverted getStyle() function back to using jQuery as the pure JavaScript version seems to be less reliable in most situations.


## [1.1.0] - 2016-05-18
### Added
- messages function for allowing you to squelch/unsquelch alerts, prompts, and confirm windows from the browser as well as reroute the normal alert output.


## [1.0.3] - 2016-05-16
### Added
- setup unit tests for all user accessible functions.
- ability to convert between rgb, rgba, and hex colors.
- a third parameter to darken() and lighten() functions to allow the 'outFormat' of the color to be changed.
- this CHANGELOG file.
- gulp build pipeline for generating the final phx plugin as well as the unit test scripts.
- hex variable to be used internally for converting to and from different color formats.
- convertColor() function, which allows you to convert between different color formats. Currently works with hex/rgb/rgba formats. Will be incorporating support for hsl/hsla formats in the future.
- getColorFormat() private function, which is used by adjustColor() and convertColor() functions to determine the format of the provided color.
- installed jQuery and Bootstrap to project as dev dependencies so getStyle() and other similar functions can be run against the popular framework to ensure that they are working fully.
- 'guid' pattern to regex array.

### Changed
- moved structure of project from one file to multiple files.
- darkenColor() function is now just darken().
- lightenColor() function is now just lighten();
- renamed index.html to buildStatus.html and set it so it will show a simple table with the result of the unit tests that are contained in the project.
- 'get' object is now 'GET' due to the all caps version being more obvious.
- 'GET' object constructor is now just a call to 'parseQS()' after the original phx initialization because with this new flow, it removes the need for having the current phx methods to be in any specific order.
- moved logic from lighten() and darken() functions to a new internal variable called adjustColor() because the logic had a lot of overlap. Adjusted the logic within adjustColor so that it can more easily be switched between lighten and darken modes and then set lighten() and darken() functions to utilize this centralized code.

### Removed
- all jQuery functions so the project will no longer have external dependencies.