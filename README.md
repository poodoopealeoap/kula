# Description
Kula is a JavaScript class library containing helper functions for making certain tasks of data manipulation easier.

# Installation
`npm install kula`

# Documentation
Once install command is complete, documentation can be found at "./node_modules/kula/Documentation.html"