'use strict';
var gulp, rename, uglify, concat, coffee, jade, del;
gulp = require('gulp');
rename = require('gulp-rename');
uglify = require('gulp-uglify');
concat = require('gulp-concat');
coffee = require('gulp-coffee');
del = require('del');

gulp.task('coffee', function() {
	return gulp.src(['src/kula.coffee', 'src/modules/**/*.coffee', 'src/init.coffee'])
		.pipe(concat('kula.coffee'))
		.pipe(coffee())
		.pipe(gulp.dest('dist'));
});

gulp.task('min', ['coffee'], function() {
	return gulp.src('dist/kula.js')
		.pipe(uglify({ mangle: false }))
		.pipe(rename('kula.min.js'))
		.pipe(gulp.dest('dist'));
});

gulp.task('watch', ['min', 'docs'], function() {
	gulp.watch('src/**/*.coffee', ['min']);
	gulp.watch('src/**/*.jade', ['docs']);
});

gulp.task('clean', function() {
	return del('dist');
});

gulp.task('default', ['min', 'docs']);